import { useContext } from "react";
// import CarsList from "./components/cars/CarsList";
import UsersList from "./components/users/UsersList";
import { CarsContext } from "./store/cars-context";

const App = () => {
  const { totalPrice } = useContext(CarsContext);

  return (
    <div>
      <p>Total cars price: {totalPrice}</p>
      {/* <CarsList /> */}
      <UsersList />
    </div>
  );
}

export default App;
