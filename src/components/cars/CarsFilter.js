import { useContext, useEffect, useState } from "react";
import { CarsContext } from "../../store/cars-context";

const CarsFilter = () => {
    const [filterValue, setFilterValue] = useState("");
    const { cars, setFilteredCars } = useContext(CarsContext);

    const filterChangeHandler = (e) => {
        setFilterValue(e.target.value);
    }
    // audi includes ""
    useEffect(() => {
        let timeout;
        if (filterValue !== "") {
            timeout = setTimeout(() => {
                console.log("FILTER")
                const filteredCars = cars.filter(car =>
                    car.car.toLowerCase().includes(filterValue.toLowerCase()));
                setFilteredCars(filteredCars);
            }, 1000);
        }

        return () => {
            console.log(typeof timeout);
            if (timeout) {
                clearTimeout(timeout);
            }
        }
    }, [filterValue, cars, setFilteredCars])

    return (
        <input onChange={filterChangeHandler} value={filterValue} />
    )
}

export default CarsFilter;