import { useContext } from "react";
import { CarsContext } from "../../store/cars-context";
import Car from "./Car";
import CarsFilter from "./CarsFilter";

const CarsList = () => {
    const { setCars, cars, filteredCars } = useContext(CarsContext);

    const fetchCarsHandler = async () => {
        try {
            const response = await fetch("https://myfakeapi.com/api/cars");
            const data = await response.json();
            setCars(data.cars);
        } catch(e) {
            console.log(e);
        }
    }

    return (
        <div>
            <CarsFilter />
            <button onClick={fetchCarsHandler}>Fetch cars</button>
            <h2>Filtered cars: </h2>
            {filteredCars.map(car => <Car key={car.id} model={car.car_model} car={car.car} year={car.car_model_year} />)}
            <h2>Cars: </h2>
            {cars.map(car => <Car key={car.id} model={car.car_model} car={car.car} year={car.car_model_year} />)}
        </div>
    )
}

export default CarsList;