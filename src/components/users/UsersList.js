import { useContext } from "react";
import { CarsContext } from "../../store/cars-context";
import { UsersContext } from "../../store/users-context";

const UsersList = () => {
    const { users, addNewUser } = useContext(UsersContext);

    const addUserHandler = () => {
        const user = {
            "id": 1001,
            "first_name": "Stefan",
            "last_name": "Stojanovic",
            "email": "acaldero0@behance.net",
            "gender": "Male",
            "birthdate": "29/12/1997",
            "company_name": "King and Sons",
            "department": "Sales",
            "job_title": "Senior Editor",
            "address": [
                {
                    "street": "1 Hanson Terrace",
                    "street_name": "Merrick",
                    "city": "Beaufort",
                    "state": "South Carolina",
                    "country": "United States",
                    "country_code": "US",
                    "postal_code": "29905"
                }
            ],
            "phone": "+7 (835) 885-9702",
            "avatar": "https://robohash.org/voluptasautmagni.png?size=180x180&set=set1",
            "email_verified": true,
            "password": "6707389d040d09a08ad2803846f30db544242f06",
            "last_login": "Never",
            "last_login_ip": "239.243.71.212",
            "subscribed": true
        };
        addNewUser(user);
    }
    return (
        <div>
            <h1>Users: </h1>
            <button onClick={addUserHandler}>Add new user</button>
            {users.map(user => <p key={user.id}>Name: {user.first_name} {user.last_name}</p>)}
        </div>
    )
}

export default UsersList;