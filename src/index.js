import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import CarsContextProvider from './store/cars-context';
import UsersContextProvider from './store/users-context';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <UsersContextProvider>
      <CarsContextProvider>
        <App />
      </CarsContextProvider>
    </UsersContextProvider>
  </React.StrictMode>
);