import React, { useEffect, useState } from "react";

export const UsersContext = React.createContext([]);

const UsersContextProvider = (props) => {
    const [users, setUsers] = useState([]);

    const fetchUsersHandler = async () => {
        try {
            const response = await fetch("https://myfakeapi.com/api/users/");
            if (response.ok) {
                const data = await response.json();
                setUsers(data.Users);
            }
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        fetchUsersHandler();
    }, [])

    const addNewUser = async (user) => {
        try {
            const response = await fetch("https://myfakeapi.com/api/users/", {
                // body: JSON.stringify(user),
                body: JSON.stringify(user),
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                }
            });
            if(response.ok) {
                setUsers(prevState => [user, ...prevState]);
                // fetchUsersHandler();
            }
        } catch (e) {
            setUsers(prevState => [user, ...prevState]);
            console.log(e);
        }
    }

    //  useEffect(() => {
    //      const fetchUsers = async () => {
    //         try {
    //             const response = await fetch("https://myfakeapi.com/api/users/");
    //             if (response.ok) {
    //                 const data = await response.json();
    //                 setUsers(data.Users);
    //             }
    //         } catch (e) {
    //             console.log(e);
    //         }
    //      }
    //      fetchUsers();
    // }, []);

    return (
        <UsersContext.Provider value={{
            users,
            fetchUsersHandler,
            addNewUser
        }}>
            {props.children}
        </UsersContext.Provider>
    )
}

export default UsersContextProvider;